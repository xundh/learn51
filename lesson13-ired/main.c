#include "led_utils.h"
#include "ired_utils.h"
#include "segment_display_utils.h"
#include "uart_utils.h"

/**
* @brief 主函数
*/
void main()
{
	// 关闭所有led
	led_all_off();
    ired_init();
    // 速率 9600
    uart_init(0xfa);
	while(1)
	{
        uart_send(0);
        uart_send(ired_data[2]);
        uart_send(1);
        segment_show_u8(0, ired_data[2]);
	}
}

#include "segment_display_utils.h"
#include "types.h"
#include "common_utils.h"

#define SMG_A_DP_PORT P0

// 3-8译码器控制端口
sbit LSA = P2^2;
sbit LSB = P2^3;
sbit LSC = P2^4;

// 这里分别是0-9,A,b,C,d,E,F,空格
u8 gsmg_code[17] =  {0x3f, 0x06, 0x5b, 0x4f,
                     0x66, 0x6d, 0x7d, 0x07,
                     0x7f, 0x6f, 0x77, 0x7c,
                     0x39, 0x5e, 0x79, 0x71, 0x40};

/**
* @brief 选择段码
*/
void select_duan(u8 duan){
    switch(duan){
        case 0:
            // 选择第一位数码管，即数码管1, 通过3-8译码器，转换为二进制码111，即7，即选中LED8
            LSA = 1; LSB = 1; LSC = 1;
            break;
        case 1:
            LSA = 0; LSB = 1; LSC = 1;
            break;
        case 2:
            LSA = 1; LSB = 0; LSC = 1;
            break;
        case 3:
            LSA = 0; LSB = 0; LSC = 1;
            break;
        case 4:
            LSA = 1; LSB = 1; LSC = 0;
            break;
        case 5:
            LSA = 0; LSB = 1; LSC = 0;
            break;
        case 6:
            LSA = 1; LSB = 0; LSC = 0;
            break;
        case 7:
            LSA = 0; LSB = 0; LSC = 0;
            break;
    }
}
/**
* @brief 数码管显示,轮流显示
*/
void segment_display(void) {
    u8 i=0;
    for(i=0;i<8;i++){
        select_duan(i);
        SMG_A_DP_PORT = gsmg_code[i];
        delay_ms(1000);
    }
}

/**
 * @brief 单个数码管显示一个数字
 * @param startPosition
 * @param num
 */
void segment_show_single_number(u8 startPosition, u8 num){
    select_duan(startPosition);
    SMG_A_DP_PORT = gsmg_code[num];
}

/**
* @brief 单个数码管显示一个数字
* @param startPosition 数码管位置,从1-8
* @param num 数码管显示数字,0-255
*/
void segment_show_u8(u8 startPosition, u8 num){
    // 先判断使用几个数码管
    u8 i;
    u8 cnt;
    // 分别计算百位 十位 个位值
    u8 bai,shi,ge;  
		if(num>99){
        cnt = 3;
    } else if(num>9){
        cnt = 2;
    } else{
        cnt = 1;
    }
    
    bai = num / 100;
    shi = (num % 100) / 10;
    ge = num % 10;
    if(cnt==3){
        select_duan(startPosition);
        SMG_A_DP_PORT = gsmg_code[bai];
        startPosition ++;
        delay_ms(1);
    }
    if(cnt>=2){
        select_duan(startPosition);
        SMG_A_DP_PORT = gsmg_code[shi];
        delay_ms(1);
        startPosition ++;
    }
    select_duan(startPosition);
    SMG_A_DP_PORT = gsmg_code[ge];
}

/**
* @brief 数码管显示 int 数值
* @param numbers 数值
*/
void segment_show_int(int numbers){
    u8 i = 0;
    u8 num[8] = {0};
    for(i=0;i<8;i++){
        num[i] = numbers % 10;
        numbers /= 10;
    }
    for(i=0;i<8;i++){
        segment_show_u8(i, num[7-i]);
        delay_ms(1);
    }
}
/**
* @brief 数码管显示 u8 数组
*/
void segment_show_u8_array(u8 *num, u8 len){
    u8 i = 0;
    for(i=0;i<len;i++){
        segment_show_u8(i, num[i]);
        delay_ms(1);
    }
}

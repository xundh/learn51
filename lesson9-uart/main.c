#include <reg52.h>
#include "led_utils.h"
#include "common_utils.h"
#include "types.h"
#include "timer_utils.h"
#include "uart_utils.h"

/**
* @brief 主函数
*/
main()
{
	// 关闭所有led
	led_all_off();
	uart_init(0xfa);
	while(1)
	{
		
	}
}

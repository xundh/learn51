#include "key_utils.h"
#include <reg52.h>
#include "common_utils.h"

sbit KEY3 = P3^2;
sbit LED1 = P2^0;

/**
 * @brief 按键中断初始化
 */
void key3_init(void){
    IT0 = 1; // 下降沿触发
    EX0 = 1; // 开启外部中断0
    EA  = 1;  // 开启总中断            

}
void exti0() interrupt 0{
    delay_10us(500);
    if(KEY3 == 0){
        LED1 = !LED1;
    }
}

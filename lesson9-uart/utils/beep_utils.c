#include "beep_utils.h"
#include "common_utils.h"

/**
 * @brief �򿪷�����
 */
void beep_on(){
    BEEP_PORT = 1;
}
/**
 * @brief �رշ�����
 */
void beep_off(){
    BEEP_PORT = 0;
}
/**
 * @brief ����������
 */
void beep_once(u16 high, u16 low){
    beep_on();
    delay(high);
    beep_off();
    delay(low);
}
/**
 * @brief ����������һ��ʱ��
*/
void beep(u16 high, u16 low, u16 times){
	u16 i;
    for(i = 0; i < times; i++){
        beep_once(high, low);
    }
}

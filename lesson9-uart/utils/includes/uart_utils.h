#ifndef __UART_UTILS_H__
#define __UART_UTILS_H__
#include "types.h"

/**
* @brief 串口初始化
*/
void uart_init(u8 baud);
/**
* @brief 串口发送数据
*/
void uart_send(u8 dat);
#endif
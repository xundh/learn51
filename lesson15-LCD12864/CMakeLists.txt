cmake_minimum_required(VERSION 3.24)
project(lesson13_ired C)

set(CMAKE_C_STANDARD 11)

include_directories(utils/includes)
include_directories(D:/Keil_v5/C51/INC)

# 添加根目录下所有层级的可执行的c文件
file(GLOB_RECURSE SOURCE_FILES utils/*.c main.c)
add_executable(lesson13_ired ${SOURCE_FILES})
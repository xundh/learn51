#include "led_utils.h"
#include "ired_utils.h"
#include "segment_display_utils.h"
#include "uart_utils.h"
#include "lcd12864_utils.h"

/**
* @brief 主函数
*/
void main()
{
	// 关闭所有led
	led_all_off();
    // 速率 9600
    uart_init(0xfa);
    lcd12864_init();
    lcd12864_clear();
    lcd12864_write_string(0, 0, "你好，中国");
	while(1)
	{
        
	}
}

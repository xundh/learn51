#include "timer_utils.h"
#include <reg51.h>
#include "types.h"

sbit LED1 = P2^0;

/**
* @brief 定时器 0 初始化函数
*/
void timer1_init(void){
    // 选择为定时器 0 模式，工作方式 1
    TMOD |= 0X01;
    // 给定时器赋初值，定时 1ms
    TH0 = 0XFC;
    TL0 = 0X66;
    // 打开定时器 0 中断允许
    ET0 = 1;
    // 打开总中断
    EA = 1;
    // 打开定时器
    TR0 = 1;
}
// 定义静态变量 i
static u16 i;

/**
* @brief 定时器 0 中断函数
*/
void timer1() interrupt 1 
{
	// 定时器重新赋初值，定时 1ms
    TH0 = 0XFC;     
    TL0 = 0X66; 
    i++;
    if(i == 1000)
    {
        i =0 ;
        LED1 = !LED1;
    }
}

//
// Created by xundh on 2024/4/2.
//

#ifndef LESSON11_DS18B20_IRED_UTILS_H
#define LESSON11_DS18B20_IRED_UTILS_H

#include <reg52.h>
#include "common_utils.h"

sbit IRED = P3^2;
extern u8 ired_data[4];

void ired_init(void);

#endif //LESSON11_DS18B20_IRED_UTILS_H

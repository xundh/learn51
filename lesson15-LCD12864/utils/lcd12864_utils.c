#include "lcd12864_utils.h"
#include "common_utils.h"

/**
* @brief  写命令
*/
void lcd12864_write_cmd(u8 cmd){
    LCD12864_RS = 0;
    LCD12864_RW = 0;
    LCD12864_EN = 0;

    LCD12864_DATAPORT = cmd;

    delay_ms(1);
    LCD12864_EN = 1;
    delay_ms(1);
    LCD12864_EN = 0;
}

/**
* @brief  写数据
*/
void lcd12864_write_data(u8 dat){
    LCD12864_RS = 1;
    LCD12864_RW = 0;
    LCD12864_EN = 0;

    LCD12864_DATAPORT = dat;

    delay_ms(1);
    LCD12864_EN = 1;
    delay_ms(1);
    LCD12864_EN = 0;
}
/**
* @brief  初始化
*/
void lcd12864_init(void){
    LCD12864_PSB = 1;
    // 功能设定，基本指令集，8位
    lcd12864_write_cmd(0x30);
    // 不使用光标， 不闪烁
    lcd12864_write_cmd(0x0c);
    // 不移动光标，不滚动
    lcd12864_write_cmd(0x06);
    // 清屏
    lcd12864_write_cmd(0x01);
}
/**
* @brief  清屏
*/
void lcd12864_clear(void){
    lcd12864_write_cmd(0x01);
}
/**
* @brief  显示字符串
*/
void lcd12864_write_string(u8 x, u8 y, u8 *str){
    if(y<0)y=0;
    if(x<0)x=0;
    if(y>3)y=3;
    x &= 0x0f;

    switch(y){
        case 0:
            x |= 0x80;
            break;
        case 1:
            x |= 0x90;
            break;
        case 2:
            x |= 0x88;
            break;
        case 3:
            x |= 0x98;
            break;
    }
    lcd12864_write_cmd(x);
    while(*str!='\0'){
        lcd12864_write_data(*str);
        str++;
    }
}

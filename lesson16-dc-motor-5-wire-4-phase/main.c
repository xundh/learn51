#include "dc_motor_5_wire_4_phase_utils.h"
#include "key_utils.h"

/**
* @brief 按键3_4回调函数
*/
void key3_4Callback(int keyNum){
	// key3 转方向 
	if(keyNum == 3){
		step_motor_28BYJ48_revert_dir();
	}else{
		// key4 增加速度
		step_motor_28BYJ48_increase_speed();
	}
}

/**
* @brief 主函数
*/
void main()
{
	step_motor_28BYJ48_set_speed(2);
	// 顺时针旋转
	step_motor_28BYJ48_set_dir(1);
	// 启动步进电机
	step_motor_28BYJ48_start();

	key3_init();
	key4_init();
	setCallback(key3_4Callback);
	while(1)
	{
		// 运行步进电机
		step_motor_28BYJ48_run();
	}		
}

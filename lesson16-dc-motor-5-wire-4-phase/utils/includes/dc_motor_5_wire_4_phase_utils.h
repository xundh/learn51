#ifndef __DC_MOTOR_5_WIRE_4_PHASE_UTILS_H__
#define __DC_MOTOR_5_WIRE_4_PHASE_UTILS_H__

#include <reg52.h>	
#include "types.h"

//定义ULN2003控制步进电机管脚
sbit IN1_A=P1^0;
sbit IN2_B=P1^1;
sbit IN3_C=P1^2;
sbit IN4_D=P1^3;

// 定义步进电机速度，值越小，速度越快
// 最小不能小于1
#define STEPMOTOR_MAXSPEED        1  
#define STEPMOTOR_MINSPEED        5  	

/**
* @brief 设置步进电机速度
* @param s 速度值，可选值1~STEPMOTOR_MAXSPEED
*/
void step_motor_28BYJ48_set_speed(u8 s);

/**
* @brief 增加步进电机速度,加到最大就从0开始
*/
void step_motor_28BYJ48_increase_speed(void);

/**
* @brief 设置步进电机方向
* @param d 方向选择,1：顺时针,0：逆时针
*/
void step_motor_28BYJ48_set_dir(u8 d);

/**
* @brief 反转步进电机方向
*/
void step_motor_28BYJ48_revert_dir(void);

/**
* @brief 启动步进电机
*/
void step_motor_28BYJ48_start(void);

/**
* @brief 运行步进电机
*/
void step_motor_28BYJ48_run(void);

/**
* @brief 停止步进电机
*/
void step_motor_28BYJ48_stop(void);
#endif

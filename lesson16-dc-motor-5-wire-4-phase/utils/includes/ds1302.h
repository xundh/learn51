//
// 时钟芯片
//

#ifndef LESSON11_DS1302_H
#define LESSON11_DS1302_H
#include <reg52.h>
#include "types.h"

sbit DS1302_CE = P3^5;
// 时钟口
sbit DS1302_CLK = P3^6;
// IO 口
sbit DS1302_IO = P3^4;

/**
* 写入一个字节
*/
void ds1302_write_byte(u8 addr, u8 dat);
/**
* 读取一个字节
*/
u8 ds1302_read_byte(u8 addr);

//变量声明
extern u8 gDS1302_TIME[7];//存储时间

void ds1302_init(void);
void ds1302_read_time(void);
#endif //LESSON11_DS1302_H

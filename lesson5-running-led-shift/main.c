#include "reg52.h"
#include "types.h"

// 定义LED的端口为P2
#define LED_PORT P2         

// 延时函数，延时一段时间
void delay_10us(u16 ten_us){
    while(ten_us--);        
}

void main(){
    int i=0;    
    while(1){   
		// 控制LED的亮灭， ~(0x01<<i) 为按位取反的操作，实现LED流水灯的效果
        LED_PORT = ~(0x01<<i);  
        i++;                
        if(i>7) i=0;        
		// 延时一段时间，控制LED的流动速度
        delay_10us(50000);  
    }
}
#include <reg52.h>
#include "led_utils.h"
#include "common_utils.h"

/**
* @brief 主函数
*/
main()
{
	// 关闭所有led
	led_all_off();

	while(1)
	{
		segment_display();
	}
}

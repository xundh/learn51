#include "common_utils.h"

/**
* 延迟函数。延迟时间=n×0.5ms
**/
void delay(int n)
{
	int i,j;
	for(j=0;j<n;j++)
		for(i=0;i<60;i++);		//循环60次约0.5ms
}
void delay_ms(int ms)
{
	int i;
	for(i=0;i<ms;i++)
		delay(2);		//调用延时函数延时1ms
}
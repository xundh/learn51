#include "led_utils.h"
#include "ired_utils.h"
#include "segment_display_utils.h"
#include "uart_utils.h"
#include "lcd1602_utils.h"

/**
* @brief 主函数
*/
void main()
{
	// 关闭所有led
	led_all_off();
    // 速率 9600
    uart_init(0xfa);
    lcd1602_init();
    lcd1602_clear();
    lcd1602_write_string(0, 0, "Hello World! Who are you?");
	while(1)
	{
        
	}
}

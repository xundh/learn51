#ifndef __SEGMENT_DISPLAY_UTILS_H__
#define __SEGMENT_DISPLAY_UTILS_H__
#include <reg52.h>
#include "types.h"

void segment_display(void);
/**
* @brief 单个数码管显示U8数值
* @param startPosition 数码管位置,从1-8
* @param num 数码管显示数字
*/
void segment_show_u8(u8 startPosition, u8 num);

/**
* @brief 数码管显示 int 数值
* @param number 数值
*/
void segment_show_int(int numbers);
/**
 * @brief 单个数码管显示一个数字
 * @param startPosition
 * @param num
 */
void segment_show_single_number(u8 startPosition, u8 num);
/**
 * @brief 数码管显示 u8 数组
 * @param num
 * @param len
 */
void segment_show_u8_array(u8 *num, u8 len);

#endif // __SEGMENT_DISPLAY_UTILS_H__
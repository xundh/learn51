#include "led_utils.h"

/**
 * @brief 打开某一个led灯
 */
void led_on(u8 led){
    LED_PORT &= ~(1 << led);
}
/**
 * @brief 关闭某一个led灯
 */
void led_off(u8 led){
    LED_PORT |= (1 << led);
}
/**
 * @brief 反转某一个led灯
*/
void led_reverse(u8 led){
    LED_PORT ^= (1 << led);
}
/**
 * @brief 打开所有led灯
*/
void led_all_on(){
    LED_PORT = 0x00;
}
/**
 * @brief 关闭所有led灯
*/
void led_all_off(){
    LED_PORT = 0xff;
}

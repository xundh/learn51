#include "lcd1602_utils.h"
#include "common_utils.h"

/**
* @brief lcd1602写命令
*/
void lcd1602_write_cmd(u8 cmd){
    // 使能
    LCD1602_EN = 0;
    // 写入命令
    LCD1602_RS = 0;
    // 写入模式
    LCD1602_RW = 0;
    // 数据口
    LCD1602_DATAPORT = cmd;
    delay_ms(1);
    // EN上升沿
    LCD1602_EN = 1;
    // 延时
    delay_ms(1);
    // EN下降沿
    LCD1602_EN = 0;
}
/**
* @brief lcd1602写数据
*/
void lcd1602_write_data(u8 dat){
    // 使能
    LCD1602_EN = 0;
    // 写入数据
    LCD1602_RS = 1;
    // 写入模式
    LCD1602_RW = 0;
    // 数据口
    LCD1602_DATAPORT = dat;
    delay_ms(1);
    // EN上升沿
    LCD1602_EN = 1;
    // 延时
    delay_ms(1);
    // EN下降沿
    LCD1602_EN = 0;
}
/**
* @brief 初始化lcd1602的GPIO
*/
void lcd1602_gpio_init(void){
    // 设置为输出
    LCD1602_RS = 0;
    LCD1602_RW = 0;
    LCD1602_EN = 0;
    LCD1602_DATAPORT = 0;
}
/**
* @brief 初始化lcd1602
*/
void lcd1602_init(void){
    // 初始化IO口
    lcd1602_gpio_init();
    // 初始化lcd1602

    // 8位数据接口，2行显示，5*7点阵
    lcd1602_write_cmd(0x38);
    // 显示器开，光标关，光标闪烁关
    lcd1602_write_cmd(0x0c);
    // 光标右移
    lcd1602_write_cmd(0x06);
    // 清屏
    lcd1602_write_cmd(0x01);
}
/**
* @brief 清屏
*/
void lcd1602_clear(void){
    lcd1602_write_cmd(0x01);
}

/**
 * 向LCD1602显示器写入一个字符串
 * @param str 要写入的字符串，以null结尾
 */
void lcd1602_write_string(u8 x, u8 y, u8 *str){
    u8 addr;
    // 显示到第几个字符
    u8 i = 0;

    // 判断x,y变量的有效性
    if(x > 15 || y > 1){
        return;
    }
    // 第一行
    if (y == 0){
        addr = 0x80 + x;
    }else{
        addr = 0xc0 + x;
    }
    lcd1602_write_cmd(addr);
    while(*str){
        if(i > 15 && y==0){
            addr = 0xc0 + x;
            i=0;
						lcd1602_write_cmd(addr);
        }else if(i>15 && y ==1){
            break;
        }
        i++;
        lcd1602_write_data(*str++);
    }
}


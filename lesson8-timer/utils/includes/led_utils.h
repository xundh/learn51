#ifndef __LED_UTILS_H__
#define __LED_UTILS_H__
#include <REG52.H>
#include "types.h"

// 定义 LED 的端口为P2
#define LED_PORT P2
/**
 * @brief 打开某一个led灯
 */
void led_on(u8 led);
/**
 * @brief 关闭某一个led灯
 */
void led_off(u8 led);
/**
 * @brief 反转某一个led灯
 */
void led_reverse(u8 led);
/**
 * @brief 打开所有led灯
 */
void led_all_on();
/**
 * @brief 关闭所有led灯
 */
void led_all_off();
#endif

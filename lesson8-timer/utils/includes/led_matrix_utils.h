#ifndef __LED_MATRIX_UTILS_H__
#define __LED_MATRIX_UTILS_H__
#include "types.h"

/**
 * @brief 74HC595显示数据
 * @param show_data 要显示的数据
 */
void hc595_show_data(u8 show_data);
void hc595_show_char(u8* array);
void led_matrix_init(void);
void hc595_show_point(u8 x, u8 y);
#endif

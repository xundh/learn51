#include "common_utils.h"

/**
 * @brief 10us延时函数
*/
void delay_10us(u16 us){
	while(us--);
}
/**
* @brief 延迟0.5ms
**/
void delay(int n)
{
	int i,j;
	for(j=0;j<n;j++)
		for(i=0;i<60;i++);		//循环60次约0.5ms
}
/**
 * @brief 延迟ms
 * @param ms 延迟时间
 */
void delay_ms(int ms)
{
	int i;
	for(i=0;i<ms;i++)
		delay(2);		//调用延时函数延时1ms
}

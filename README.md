# 51单片机学习

博客地址：
https://blog.csdn.net/xundh/category_12110844.html
https://www.zhihu.com/column/c_1751606807754461184

- lesson4-new-project 新工程示例
- lesson5-running-led-shift 移位实现流水灯
- lesson5-running-led-lib 库实现移位
- lesson5-beep 蜂鸣器
- lesson6- 数码管显示
- lesson7-serial-2-paraller 串转并 LED点阵显示
- lesson7-show-char LED点阵显示字符
- lesson8-key-led-irq 按键中断
- lesson8-timer 定时器
- lesson9-uart 串口通讯
- lesson10-iic-eeprom i2c通讯与存储
- lesson11-ds18b20 1-wire通讯,温度传感器，忽略了小数点
- lesson12-ds1302-clock 时钟芯片使用
- lesson13-ired 红外遥控接收
- lesson14-LCD1602 液晶屏
- lesson15-LCD12864 液晶屏
- lesson16-dc-motor 直流马达
- lesson16-dc-motor-5-wire-4-phase 五线四相步进电机
- lesson17-motor-4-wire 四线双极性步进电机控制
- lesson18-motor-sg90 sg90舵机控制
- 
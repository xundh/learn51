#include <reg52.h>
#include "led_utils.h"
#include "common_utils.h"
#include "led_matrix_utils.h"
#include "types.h"
// 数据0
u8 gled_row[8] = {0x00, 0x7c, 0x82, 0x82, 0x82, 0x7c, 0x00, 0x00};
/**
* @brief 主函数
*/
main()
{
	// 关闭所有led
	led_all_off();

	led_matrix_init();
	while(1)
	{
		hc595_show_char(gled_row);
	}
}

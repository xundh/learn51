#include "led_utils.h"
#include "dc_motor_utils.h"
#include "common_utils.h"

/**
* @brief 主函数
*/
void main()
{
	// 关闭所有led
	led_all_off();

	while(1)
	{
		dc_motor_start();
		delay_ms(1000*5);
		dc_motor_stop();
		delay_ms(1000*5);
	}
}

//
// 时钟芯片
//
#include "ds1302.h"
#include "intrins.h"

// DS1302写入和读取的地址命令
u8 gREAD_RTC_ADDR[7] = {
    0x81,   // 二进制  1000 0001 , 表示: 1000 0000 读取秒
    0x83,   // 1000 0011 读取分 
    0x85,   // 1000 0101 读取时
    0x87,   // 1000 0111 读取日
    0x89,   // 1000 1001 读取月
    0x8b,   // 1000 1011 读取星期
    0x8d    // 1000 1101 读取年
    };
u8 gWRITE_RTC_ADDR[7] = {0x80, 0x82, 0x84, 0x86, 0x88, 0x8a, 0x8c};
// DS1302 初始化要写入的时间
u8 gDS1302_TIME[7] = {
    0x47,   // 秒
    0x05,   // 分
    0x11,   // 时
    0x01,   // 日
    0x04,   // 月
    0x05,   // 星期
    0x24    // 年
    };

/**
* 写入一个字节
*/
void ds1302_write_byte(u8 addr, u8 dat){
    u8 i;
    
    // 使能脚复位
    DS1302_CE = 0;
    _nop_();
    // 时钟脚复位
    DS1302_CLK = 0;
    _nop_();
    // 使能脚置高
    DS1302_CE = 1;
    _nop_();
    // 从低位开始写入
    for(i=0; i<8; i++){
        // 先发送地址
        DS1302_IO = addr & 0x01;
         // 右移一位
        addr >>= 1;

        // SLK 上升沿写入
        DS1302_CLK = 1;
        _nop_();
        DS1302_CLK = 0;
        _nop_();
    }
    // 写入数据
    for(i=0; i<8; i++){
        // 先发送数据
        DS1302_IO = dat & 0x01;
        // 右移一位
        dat >>= 1;
        // SLK 上升沿写入
        DS1302_CLK = 1;
        _nop_();
        DS1302_CLK = 0;
        _nop_();
    }
    // 复位
    DS1302_CE = 0;
    _nop_();
}
/**
* 读取一个字节
*/
u8 ds1302_read_byte(u8 addr){
    u8 i;
    u8 temp = 0;
    u8 value = 0;
    // 使能脚复位
    DS1302_CE = 0;
    _nop_();
    // 时钟脚复位
    DS1302_CLK = 0;
    _nop_();
    // 使能脚置高
    DS1302_CE = 1;
    _nop_();
    // 从低位开始写入
    for(i=0; i<8; i++){
        // 先发送地址
        DS1302_IO = addr & 0x01;
        // 右移一位
        addr >>= 1;
        // SLK 上升沿写入
        DS1302_CLK = 1;
        _nop_();
        DS1302_CLK = 0;
        _nop_();
    }
    // 读取数据
    for(i=0; i<8; i++){
        temp = DS1302_IO;
        value = (temp << 7) | (value >> 1);
        // SLK 下降沿读取
        DS1302_CLK = 1;
        _nop_();
        DS1302_CLK = 0;
        _nop_();
    }
    // 复位
    DS1302_CE = 0;
    _nop_();

    // 释放时钟
    DS1302_CLK = 1;

    _nop_();
    DS1302_IO = 0;
    _nop_();
    DS1302_IO = 1;
    _nop_();
    return value;
}
/**
* ds1302初始化
*/
void ds1302_init(void){
    u8 i = 0;
    // 写入禁止写保护
    ds1302_write_byte(0x8e, 0x00);
    // 写数据
    for(i=0; i<7; i++){
        ds1302_write_byte(gWRITE_RTC_ADDR[i], gDS1302_TIME[i]);
    }
    // 写入启用写保护
    ds1302_write_byte(0x8e, 0x80);
}
/**
* 写入时间
*/
void ds1302_read_time(void){
	u8 i=0;
	for(i=0;i<7;i++)
	{
		gDS1302_TIME[i]=ds1302_read_byte(gREAD_RTC_ADDR[i]);	
	}	
}

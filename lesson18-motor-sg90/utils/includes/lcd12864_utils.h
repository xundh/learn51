#ifndef __LCD12864_UTILS_H__
#define __LCD12864_UTILS_H__
#include <reg52.h>
#include "types.h"

// 管脚定义 
sbit LCD12864_RS = P2^6;
// RW 读写选择
sbit LCD12864_RW = P2^5;
// EN 使能端
sbit LCD12864_EN = P2^7;
// 8位还是4位
sbit LCD12864_PSB = P3^2;
// 数据总线
#define LCD12864_DATAPORT P0

/**
* @brief 写数据
*/
void lcd12864_write_cmd(u8 cmd);
/**
* @brief 初始化
*/
void lcd12864_init(void);
/**
* @brief  清屏
*/
void lcd12864_clear(void);
/**
* @brief  显示字符串
*/
void lcd12864_write_string(u8 x, u8 y, u8 *str);
#endif
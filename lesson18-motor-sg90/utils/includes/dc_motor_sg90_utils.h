#ifndef __DC_MOTOR_SG90_UTILS_H__
#define __DC_MOTOR_SG90_UTILS_H__
#include <reg52.h>
#include "types.h"

               

void sg90_timer_Init();
void sg90_run(void);

#endif

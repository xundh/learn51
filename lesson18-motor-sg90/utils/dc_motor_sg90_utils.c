#include "dc_motor_sg90_utils.h"
 
// 信号引脚
sbit SG_PWM = P0^0;
// 计数用来切换高低电平
static u8 PWM_count=3;   
static u8 count=0;
 //1--0度，2--45度，3--90度，4--135度，5--180度
static u8 sg90_angle=0;
void sg90_timer_Init()
{
	TMOD = 0X01;      	// T0定时方式1
	TH0 = 0Xfe;
	TL0 = 0X33;       	// 计数初值设置为0.5ms
	ET0=1;          	// 打开定时器0的中断
	TR0=1;          	// 打开定时器0
	EA=1;           	// 开总中断
}
// 定时器中断0
void sg90_timer_irq() interrupt 1
{
	TR0=0;
	TH0 = 0XFE;
	TL0 = 0X33;       //重新赋计数初值为0.5ms
	if(count <= PWM_count){
		SG_PWM=1;
	}
	else{
		SG_PWM=0;
	}
	count++;
	if(count>=40){
		count=0;
		sg90_angle++;
	}
	TR0=1;
}
void sg90_run(void){
	if(sg90_angle==16){
		PWM_count=1;
	}
	if(sg90_angle==32){
		PWM_count=3;
		sg90_angle=0;
	}
}
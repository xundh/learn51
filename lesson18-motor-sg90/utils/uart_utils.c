#include "uart_utils.h"
#include <reg52.h>

/**
* @brief 串口初始化
*/
void uart_init(u8 baud){
    // 设置计数器1的工作方式2
    TMOD = 0x20;
    // 设置定时器1的工作方式1
    SCON = 0x50; // 0b01010000 
    // 波特率倍频
    PCON = 0x80; // 0b10000000
    // 计数器初始值
    TH1 = baud;
    TL1 = baud;
    // 打开接收中断
    ES = 1; 
    // 打开总中断
    EA = 1;
    // 启动定时器1
    TR1 = 1;
}
/**
* @brief 串口发送数据
*/
void uart_send(u8 dat){
    SBUF = dat;
    while(!TI);
    TI = 0;
}
/**
* @brief 使用中断接收串口数据
*/
void uart_recv() interrupt 4{
    u8 dat;
    if(RI){
        RI = 0;
        dat = SBUF;
        uart_send(dat);
    }
}

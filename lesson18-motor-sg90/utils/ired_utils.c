//
// Created by xundh on 2024/4/2.
//
#include "ired_utils.h"

u8 ired_data[4];
/**
 * @brief ired 初始化
 */
void ired_init(void){
    // 使能外部中断0
    IT0 = 1;
    // 设置外部中断0下降沿触发
    EX0 = 1;
    // 使能总中断
    EA = 1;
    // 拉高IRED
    IRED = 1;
}


/**
 * @brief ired 接收中断
 */
void ired() interrupt 0{
    u8 i,j;
    u16 time_cnt ;
    // 保存高电平时间
    u16 high_cnt;
    // 是不是低电平
    if(IRED == 0){
        time_cnt = 1000;
        // 高电平时退出
        while((!IRED) && time_cnt){
            delay_10us(1);
            time_cnt--;
        }
        if(time_cnt==0)return;
        // ired接收到信号, 10ms内进入高电平

        if(IRED){
            // 4.5ms高电平结束，超过5ms就是错误信号
            time_cnt = 500;
            while(IRED && time_cnt){
                delay_10us(1);
                time_cnt--;
            }
            if(time_cnt==0)return;

            // 接收数据,地址码、地址反码、控制码、控制反码
            for(i=0;i<4;i++){
                for(j=0;j<8;j++){
                    // 0.56ms低电平(这里使用600us)
                    time_cnt = 600;
                    while(IRED==0 && time_cnt){
                        delay_10us(1);
                        time_cnt--;
                    }
                    // 大于560us，非正常信号
                    if(time_cnt==0)return;

                    // 判断高电平时间,0  560us, 1680us
                    time_cnt = 20;
                    high_cnt = 0;
                    while(IRED && time_cnt){
                        delay_10us(10);
                        time_cnt--;
                        high_cnt++;
                    }
                    // 超时
                    if(time_cnt==0)return;
                    ired_data[i] >>= 1;

                    // 大于800us就认为是1
                    if(high_cnt>=8){
                        // 高电平
                        ired_data[i] |= 0x80;
                    }
                }
            }
            // 反码判断数据是否正确
            if(ired_data[2] != ~ired_data[3] ){
                // 数据错误
                for(i=0;i<4;i++){
                    ired_data[i] = 0;
                }
            }
        }
    }
}

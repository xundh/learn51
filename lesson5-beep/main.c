#include <reg52.h>
#include "beep_utils.h"
#include "led_utils.h"
#include "common_utils.h"

/**
* @brief 主函数
*/
main()
{
	// 关闭所有led
	led_all_off();
	// 打开第1个led
	led_on(0);
	led_on(7);

	beep(10, 10, 100);
	while(1)
	{
		delay(10000);
	}
}

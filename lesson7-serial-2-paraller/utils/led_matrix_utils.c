#include "led_matrix_utils.h"
#include "common_utils.h"

// 时钟输入引脚，用于控制数据的移位操作
sbit SR_CLK = P3^6;
// 数据数据推到输出寄存器的引脚
sbit R_CLK = P3^5;
// 串行数据输入
sbit SER = P3^4;

u8 gc595_buf[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};

// 低电平有效，开启列
#define LED_MATRIX_PORT P0

void hc595_show_data(u8 show_data)
{
    u8 i = 0;
    for (i = 0; i < 8; i++)
    {
        SER = show_data >> 7 ;
        show_data <<= 1;
        // 移位寄存器时钟上升沿,进行移位操作
        SR_CLK = 0;
        delay_10us(1);
        SR_CLK = 1;
        delay_10us(1);
    }
    // 输出寄存器时钟上升沿,将移位寄存器的数据复制到输出寄存器
    R_CLK = 0;
    delay_10us(1);
    R_CLK = 1;
    delay_10us(1);
}
void hc595_show_row(void){
    u8 i=0;
    LED_MATRIX_PORT = 0;
    for(i=0;i<8;i++){
        hc595_show_data(0x00);
        hc595_show_data(gc595_buf[i]);
        delay_ms(500);
    }
}
void hc595_show_column(void){
    u8 i=0;
    hc595_show_data(0xFF);
    for(i=0;i<8;i++){
        LED_MATRIX_PORT = ~gc595_buf[i];
        delay_ms(500);
    }
}
void hc595_show_point(u8 x, u8 y){
    // 把 LED_MATRIX_PORT 对应的x位置设置为0
    x = 8-x;
    y = 8-y;
    LED_MATRIX_PORT = ~(0x01 << x);

    hc595_show_data(gc595_buf[x]);
    delay_ms(1000);
}

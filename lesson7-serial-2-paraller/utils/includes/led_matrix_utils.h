#ifndef __LED_MATRIX_UTILS_H__
#define __LED_MATRIX_UTILS_H__
#include "types.h"
#include <reg52.h>

/**
 * @brief 74HC595显示数据
 * @param show_data 要显示的数据
 */
void hc595_show_data(u8 show_data);
void hc595_show_row(void);
void hc595_show_column(void);
void hc595_show_point(u8 x, u8 y);
void hc595_show_array(u8 *show_data);
#endif

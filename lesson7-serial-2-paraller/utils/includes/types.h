#ifndef __TYPES_H__
#define __TYPES_H__

// 定义无符号8位整型数据类型
typedef unsigned char u8;  
// 定义无符号16位整型数据类型
typedef unsigned int u16;   

#endif
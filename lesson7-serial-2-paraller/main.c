#include <reg52.h>
#include "led_utils.h"
#include "common_utils.h"
#include "led_matrix_utils.h"

/**
* @brief 主函数
*/
main()
{
	// 关闭所有led
	led_all_off();

	while(1)
	{
		// 显示一个点
		hc595_show_point(3, 3);
		// 延迟2s
		delay_ms(2000);
		// 滚动显示行
		hc595_show_row();
		// 滚动显示列
		hc595_show_column();
	}
}

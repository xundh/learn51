#include <reg52.h>
#include "led_utils.h"
#include "common_utils.h"
#include "timer_utils.h"
#include "uart_utils.h"
#include "key_utils.h"
#include "eeprom_utils.h"
#include "segment_display_utils.h"
#include "ds18b20.h"

/**
* @brief 主函数
*/
main()
{
	float temperature = 0;
	int intTemperature = 0;
	u8 i =0;
	// 关闭所有led
	led_all_off();
	uart_init(0xFA);
	ds18b20_init();
	
	while(1)
	{
		i++;
		if(i % 50==0){
			temperature = ds18b20_get_temperature();
			intTemperature = temperature * 10;
		}
		segment_show_int(intTemperature);
	}
}

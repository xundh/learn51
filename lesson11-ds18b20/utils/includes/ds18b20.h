#ifndef __DS18B20_H__
#define __DS18B20_H__

#include "common_utils.h"
#include "types.h"
#include <reg52.h>

// DS18B20数据口定义
sbit DS18B20_PORT = P3^7;

/**
* @brief DS18B20初始化
* @return 0:成功 1:失败
*/
u8 ds18b20_init(void);


/**
* @brief DS18B20读取温度
* @return 温度值
*/
float ds18b20_get_temperature(void);
#endif

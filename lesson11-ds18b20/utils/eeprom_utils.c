#include "eeprom_utils.h"
#include "i2c_utils.h"
#include "common_utils.h"

/**
* @brief EEPROM 写入数据
*/
void at24c02_write(u8 addr,u8 dat)
{
    i2c_start();
    // a0=1010 0000 ，1010是固定的，0000是地址，这里是写入地址
    i2c_send_byte(0xa0);
    i2c_wait_ack();
    i2c_send_byte(addr);
    i2c_wait_ack();
    i2c_send_byte(dat);
    i2c_wait_ack();
    i2c_stop();
    delay_ms(10);	 
}
/**
* @brief EEPROM 读取数据
*/
u8 at24c02_read(u8 addr)
{
    u8 dat;
    i2c_start();
    // 发送器件地址和写控制位
    i2c_send_byte(0xa0);
    i2c_wait_ack();
    // 写入要读取的地址
    i2c_send_byte(addr);
    i2c_wait_ack();
    // 改变传送方向,读写信号反过来,重新启动
    i2c_start();
    // 发送器件地址和读控制位
    i2c_send_byte(0xa1);
    i2c_wait_ack();
    // 读取数据
    dat=i2c_read_byte(0);
    i2c_stop();
    return dat;
}

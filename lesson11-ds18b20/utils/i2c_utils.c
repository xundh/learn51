#include "i2c_utils.h"
#include "common_utils.h"

/**
* @brief I2C 起始信号
*/
void i2c_start(void)
{
	IIC_SDA=1;  //如果把该条语句放在SCL后面，第二次读写会出现问题
	delay_10us(1);
	IIC_SCL=1;
	delay_10us(1);
	IIC_SDA=0;	//当SCL为高电平时，SDA由高变为低
	delay_10us(1);
	IIC_SCL=0;  //钳住I2C总线，准备发送或接收数据
	delay_10us(1);
}
/**
* @brief I2C 停止信号
*/
void i2c_stop(void)
{
	IIC_SDA=0;
	delay_10us(1);
	IIC_SCL=1;
	delay_10us(1);
	IIC_SDA=1;
	delay_10us(1);			
}

/**
* @brief I2C 应答信号
*/
void i2c_ack(void)
{
	IIC_SCL=0;
	IIC_SDA=0;	//SDA为低电平
	delay_10us(1);
   	IIC_SCL=1;
	delay_10us(1);
	IIC_SCL=0;
}
/**
* @brief I2C 非应答信号
*/
void i2c_nack(void)
{
	IIC_SCL=0;
	IIC_SDA=1;	//SDA为高电平
	delay_10us(1);
   	IIC_SCL=1;
	delay_10us(1);
	IIC_SCL=0;	
}
/**
* @brief 主机等待从机应答
*/
u8 i2c_wait_ack(void){
    u8 ucErrTime=0;
    // 保持 SCL 高电平
    IIC_SCL=1;
    delay_10us(1);
    while(IIC_SDA){
        ucErrTime++;
        // 如果等待时间过长，返回错误
        if(ucErrTime>100){
            i2c_stop();
            return 1;
        }
    }
    IIC_SCL=0;
    return 0;
}
/**
* @brief I2C 发送一个字节
*/
void i2c_send_byte(u8 dat){
    u8 t;
    // 低电平时 可以SDA可以改变
    IIC_SCL=0;
    for(t=0;t<8;t++){
        // 最高位是1
        if((dat & 0x80)>0){
            // 发送 1
            IIC_SDA=1;
        }else{
            // 发送 0
            IIC_SDA=0;
        }
        // 下一位
        dat<<=1;
        delay_10us(1);
        // 时序
        IIC_SCL = 1;
        delay_10us(1);
        IIC_SCL = 0;
        delay_10us(1);
    }
}
/**
* @brief I2C 读取一个字节
* @param ack 0:非应答 1:应答
*/
u8 i2c_read_byte(u8 ack){
    u8 i,receive=0;
    for(i=0;i<8;i++){
        IIC_SCL=0;
        delay_10us(1);
        // 数据不能变了
        IIC_SCL=1;
        // 读前要移位, 从高位开始
        receive<<=1;
        if(IIC_SDA)receive++;
        delay_10us(1);
    }
    if (!ack)
        i2c_nack();
    else
        i2c_ack();
    return receive;
}

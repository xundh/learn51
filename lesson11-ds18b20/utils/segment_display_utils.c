#include "segment_display_utils.h"
#include "types.h"
#include "common_utils.h"

#define SMG_A_DP_PORT P0

// 3-8译码器控制端口
sbit LSA = P2^2;
sbit LSB = P2^3;
sbit LSC = P2^4;

// 这里分别是0-9,A,b,C,d,E,F,空格
u8 gsmg_code[17] =  {0x3f, 0x06, 0x5b, 0x4f,
                     0x66, 0x6d, 0x7d, 0x07,
                     0x7f, 0x6f, 0x77, 0x7c,
                     0x39, 0x5e, 0x79, 0x71, 0x00};

/**
* @brief 选择段码
*/
void select_duan(u8 duan){
    switch(duan){
        case 0:
            // 选择第一位数码管，即数码管1, 通过3-8译码器，转换为二进制码111，即7，即选中LED8
            LSA = 1; LSB = 1; LSC = 1;
            break;
        case 1:
            LSA = 0; LSB = 1; LSC = 1;
            break;
        case 2:
            LSA = 1; LSB = 0; LSC = 1;
            break;
        case 3:
            LSA = 0; LSB = 0; LSC = 1;
            break;
        case 4:
            LSA = 1; LSB = 1; LSC = 0;
            break;
        case 5:
            LSA = 0; LSB = 1; LSC = 0;
            break;
        case 6:
            LSA = 1; LSB = 0; LSC = 0;
            break;
        case 7:
            LSA = 0; LSB = 0; LSC = 0;
            break;
    }
}
/**
* @brief 数码管显示,轮流显示
*/
void segment_display() {
    u8 i=0;
    for(i=0;i<8;i++){
        select_duan(i);
        SMG_A_DP_PORT = gsmg_code[i];
        delay_ms(1000);
    }
}

/**
* @brief 单个数码管显示一个数字
* @param position 数码管位置,从1-8
* @param num 数码管显示数字
*/
void segment_show_u8(u8 position, u8 num){
    select_duan(position);
    SMG_A_DP_PORT = gsmg_code[num];
}

/**
* @brief 数码管显示 int 数值
* @param numbers 数值
*/
void segment_show_int(int numbers){
    u8 i = 0;
    u8 num[8] = {0};
    for(i=0;i<8;i++){
        num[i] = numbers % 10;
        numbers /= 10;
    }
    for(i=0;i<8;i++){
        segment_show_u8(i, num[7-i]);
        delay_ms(1);
    }
}

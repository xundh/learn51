#include "reg52.h"

sbit LED1 = P2^0;
sbit LED2 = P2^1;
void delay_10us(int ten_us){
 	while(ten_us--);
}
void main(){
    while(1){
		LED1=0;
		delay_10us(1000*1000);
		LED1=1;
		delay_10us(1000*1000);
		LED2=0;
		delay_10us(1000*1000);
		LED2=1;
		delay_10us(1000*1000);
	}
}

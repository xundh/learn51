#include "dc_motor_4_wire_utils.h"
#include "key_utils.h"

/**
* @brief 按键3_4回调函数
*/
void key3_4Callback(int keyNum){
	// key3 转方向 
	if(keyNum == 3){
		step_motor_4_wire_revert_dir();
	}else{
		// key4 增加速度
		step_motor_4_wire_increase_speed();
	}
}

/**
* @brief 主函数
*/
void main()
{
	step_motor_4_wire_init();
	step_motor_4_wire_set_speed(1);
	// 顺时针旋转
	step_motor_4_wire_set_dir(1);
	// 启动步进电机
	step_motor_4_wire_start();

	key3_init();
	key4_init();
	setCallback(key3_4Callback);
	while(1){
		// 运行步进电机
		step_motor_4_wire_run();
	}		
}

#include "key_utils.h"
#include <reg52.h>
#include "common_utils.h"

/**
 * @brief 按键中断初始化
 */
void key3_init(void){
    IT0 = 1; // 下降沿触发
    EX0 = 1; // 开启外部中断0
    EA  = 1;  // 开启总中断            
}
void key4_init(void){
    IT1 = 1; // 下降沿触发
    EX1 = 1; // 开启外部中断1
    EA  = 1;  // 开启总中断            
}
// key3 中断
void exti0() interrupt 0{
    delay_10us(500);
    if(KEY3 == 0 && callback != NULL){
        callback(3);
    }
}
// key4 中断
void exti1() interrupt 2{
    delay_10us(500);
    if(KEY4 == 0 && callback != NULL){
        callback(4);
    }
}
void setCallback(CallbackFunction cb){
    callback = cb;
}

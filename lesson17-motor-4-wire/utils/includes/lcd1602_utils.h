#ifndef __LCD1602_UTILS__H__
#define __LCD1602_UTILS__H__
#include <reg52.h>
#include "types.h"

// 寄存器选择
sbit LCD1602_RS = P2^6;
// RW 读写选择
sbit LCD1602_RW = P2^5;
// EN 使能端
sbit LCD1602_EN = P2^7;
// 数据总线
#define LCD1602_DATAPORT P0

void lcd1602_write_cmd(u8 cmd);
void lcd1602_write_data(u8 dat);
/**
* @brief 初始化lcd1602
*/
void lcd1602_init();
/**
* @brief 清屏
*/
void lcd1602_clear(void);
void lcd1602_write_string(u8 x, u8 y, u8 *str);
#endif

#ifndef __DC_MOTOR_4_WIRE_UTILS_H__
#define __DC_MOTOR_4_WIRE_UTILS_H__
#include <reg52.h>
#include "types.h"
#include "common_utils.h"

// OUT_A
sbit IN_A = P1^0;
// OUT_B
sbit IN_B = P1^1;
// OUT_C
sbit IN_C = P1^2;
// OUT_D
sbit IN_D = P1^3;

// 定义步进电机速度，值越小，速度越快
// 最小不能小于1
#define STEPMOTOR_MAXSPEED        1  
#define STEPMOTOR_MINSPEED        5  	

/**
* @brief 设置速度
*/
void step_motor_4_wire_set_speed(u8 s);
/**
* @brief 增加速度
*/
void step_motor_4_wire_increase_speed(void);
/**
* @brief 设置方向
*/
void step_motor_4_wire_set_dir(u8 d);
/**
* @brief 反转方向
*/
void step_motor_4_wire_revert_dir(void);
/**
* @brief 启动
*/
void step_motor_4_wire_start(void);
/**
* @brief 运行
*/
void step_motor_4_wire_run(void);
/**
* @brief 停止
*/
void step_motor_4_wire_stop(void);

void step_motor_4_wire_init(void);
#endif

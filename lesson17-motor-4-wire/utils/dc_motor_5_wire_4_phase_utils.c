#include "dc_motor_5_wire_4_phase_utils.h"
#include "common_utils.h"

// 默认逆时针方向
static u8 dir=0;
// 默认最大速度旋转
static u8 speed=STEPMOTOR_MAXSPEED;
static u8 step=0;
// 1运行中 0停止运行
static u8 run_flag=0;

/**
* @brief 向步进电机发送一个脉冲
* @param step 指定步进序号，可选值0~7
* @param dir 方向选择,1：顺时针,0：逆时针
*/
void step_motor_28BYJ48_send_pulse(void){
	u8 temp=step;
	
	if(dir==0)	//如果为逆时针旋转
		temp=7-step;//调换节拍信号
	switch(temp)//8个节拍控制：A->AB->B->BC->C->CD->D->DA
	{
		case 0: IN1_A=1;IN2_B=0;IN3_C=0;IN4_D=0;break;
		case 1: IN1_A=1;IN2_B=1;IN3_C=0;IN4_D=0;break;
		case 2: IN1_A=0;IN2_B=1;IN3_C=0;IN4_D=0;break;
		case 3: IN1_A=0;IN2_B=1;IN3_C=1;IN4_D=0;break;
		case 4: IN1_A=0;IN2_B=0;IN3_C=1;IN4_D=0;break;
		case 5: IN1_A=0;IN2_B=0;IN3_C=1;IN4_D=1;break;
		case 6: IN1_A=0;IN2_B=0;IN3_C=0;IN4_D=1;break;
		case 7: IN1_A=1;IN2_B=0;IN3_C=0;IN4_D=1;break;
		default: IN1_A=0;IN2_B=0;IN3_C=0;IN4_D=0;break;//停止相序	
	}			
}
/**
* @brief 设置步进电机速度
* @param s 速度值，可选值1~STEPMOTOR_MAXSPEED
*/
void step_motor_28BYJ48_set_speed(u8 s){
    speed=s;
}
/**
* @brief 增加步进电机速度,加到最头0开始
*/
void step_motor_28BYJ48_increase_speed(void){
    if(speed>STEPMOTOR_MAXSPEED)speed--;
    else{
        speed = STEPMOTOR_MINSPEED;
    }
}
/**
* @brief 设置步进电机方向
* @param d 方向选择,1：顺时针,0：逆时针
*/
void step_motor_28BYJ48_set_dir(u8 d){
    dir=d;
}
/**
* @brief 反转步进电机方向
*/
void step_motor_28BYJ48_revert_dir(void){
    dir=!dir;
}

/**
* @brief 启动步进电机
*/
void step_motor_28BYJ48_start(void){
    run_flag=1;
}
/**
* @brief 运行步进电机
*/
void step_motor_28BYJ48_run(void){
    if(run_flag==0)return;
    step_motor_28BYJ48_send_pulse();
		step++;
    if(step>7)step=0;
	delay_ms(speed);
}
/**
* @brief 停止步进电机
*/
void step_motor_28BYJ48_stop(void){
    run_flag=0;
}
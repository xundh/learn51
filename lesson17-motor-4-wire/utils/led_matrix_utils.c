#include "led_matrix_utils.h"
#include "common_utils.h"
#include <reg52.h>

// 时钟输入引脚，用于控制数据的移位操作
sbit SR_CLK = P3^6;
// 数据数据推到输出寄存器的引脚
sbit R_CLK = P3^5;
// 串行数据输入
sbit SER = P3^4;

u8 gled_column[8] = {0x7f, 0xbf, 0xdf, 0xef, 0xf7, 0xfb, 0xfd, 0xfe};

// 低电平有效，开启列
#define LED_MATRIX_PORT P0

void led_matrix_init(void){
    LED_MATRIX_PORT = 0x00;
}
void hc595_show_data(u8 show_data)
{
    u8 i = 0;
    for (i = 0; i < 8; i++)
    {
        SER = show_data >> 7 ;
        show_data <<= 1;
        // 移位寄存器时钟上升沿,进行移位操作
        SR_CLK = 0;
        delay_10us(1);
        SR_CLK = 1;
    }
    // 输出寄存器时钟上升沿,将移位寄存器的数据复制到输出寄存器
    R_CLK = 0;
    delay_10us(1);
    R_CLK = 1;
}
void hc595_show_char(u8* gled_row){
    u8 i =0;
    for(i=0;i<8;i++){
        LED_MATRIX_PORT = gled_column[i];
        hc595_show_data(gled_row[i]);
        delay_10us(280);
        hc595_show_data(0x00);
    }
}

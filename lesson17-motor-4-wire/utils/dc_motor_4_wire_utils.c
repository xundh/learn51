#include "dc_motor_4_wire_utils.h"

// 默认逆时针方向
static u8 dir=0;
// 默认最大速度旋转
static u8 speed=STEPMOTOR_MAXSPEED;
static u8 step=0;
// 1运行中 0停止运行
static u8 run_flag=0;
/**
* @brief 向步进电机发送一个脉冲
* @param step 指定步进序号，可选值0~7
* @param dir 方向选择,1：顺时针,0：逆时针
*/
void step_motor_4_wire_send_pulse(void){
	u8 temp=step;
	
	if(dir==0)	//如果为逆时针旋转
		temp=7-step;//调换节拍信号

  // (A+)---(A+B+)---(B+)--(B+A-)---(A-)---(A-B-)---(B-)---(B-A+)
	// A-AC-C-CB-B-BD-D-DA-A
	switch(temp)
	{   //        黑A+  绿A-    黄B+   红B-
		case 0: IN_A=1;IN_B=0;IN_C=0;IN_D=0;break;
		case 1: IN_A=1;IN_B=0;IN_C=1;IN_D=0;break;
		case 2: IN_A=0;IN_B=0;IN_C=1;IN_D=0;break;
		case 3: IN_A=0;IN_B=1;IN_C=1;IN_D=0;break;
		case 4: IN_A=0;IN_B=1;IN_C=0;IN_D=0;break;
		case 5: IN_A=0;IN_B=1;IN_C=0;IN_D=1;break;
		case 6: IN_A=0;IN_B=0;IN_C=0;IN_D=1;break;
		case 7: IN_A=1;IN_B=0;IN_C=0;IN_D=1;break;
        //default: IN_A=0;IN_B=0;IN_C=0;IN_D=0;break;
	}			
}
/**
* @brief 设置速度
*/
void step_motor_4_wire_set_speed(u8 s){
    speed=s;
}
/**
* @brief 增加速度
*/
void step_motor_4_wire_increase_speed(void){
    if(speed>STEPMOTOR_MAXSPEED)speed--;
    else{
        speed = STEPMOTOR_MINSPEED;
    }
}
/**
* @brief 设置方向
*/
void step_motor_4_wire_set_dir(u8 d){
    dir=d;
}
/**
* @brief 反转方向
*/
void step_motor_4_wire_revert_dir(void){
	if(dir==0)
		dir=1;
	else
		dir=0;
}
/**
* @brief 启动
*/
void step_motor_4_wire_start(void){
    run_flag=1;
}
/**
* @brief 运行
*/
void step_motor_4_wire_run(void){

	if(run_flag==1){
		step_motor_4_wire_send_pulse();
		step++;
		if(step>7)step=0;
		delay_10us(speed*300);
	}
}
/**
* @brief 停止
*/
void step_motor_4_wire_stop(void){
    run_flag=0;
}
void step_motor_4_wire_init(void){
	IN_A=0;
	IN_B=0;
	IN_C=0;
	IN_D=0;
}

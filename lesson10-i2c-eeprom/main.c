#include <reg52.h>
#include "led_utils.h"
#include "common_utils.h"
#include "types.h"
#include "timer_utils.h"
#include "uart_utils.h"
#include "key_utils.h"
#include "eeprom_utils.h"
#include "types.h"

static u8 i = 0;
/**
* @brief 按键3回调函数
*/
void key3_4Callback(int keyNum){
	u8 dat;
	
	if(keyNum == 3){
		LED1 = 1;
		at24c02_write(0x00, i);
		uart_send(i);
		i++;
	}else{
		LED1 = 0;
		// 读取数据
		dat = at24c02_read(0x00);
		uart_send(dat);
	}
}

/**
* @brief 主函数
*/
main()
{
	// 关闭所有led
	led_all_off();
	key3_init();
	key4_init();
	uart_init(0xFA);
	
	setCallback(key3_4Callback);

	while(1)
	{
		
	}
}

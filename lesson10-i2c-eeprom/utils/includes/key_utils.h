#ifndef __KEY_UTILS_H__
#define __KEY_UTILS_H__
#include "stdlib.h"
#include <reg52.h>

// 定义4个按键
sbit KEY1 = P3^0;
sbit KEY2 = P3^1;
sbit KEY3 = P3^2;
sbit KEY4 = P3^3;

/**
* @brief 定义回调函数指针
*/
typedef void (*CallbackFunction)(int);
/**
* @brief 设置回调函数
*/
static CallbackFunction callback = NULL;
/**
* @brief 给回调函数赋值
*/
void setCallback(CallbackFunction cb);

void key3_init(void);
void key4_init(void);
#endif

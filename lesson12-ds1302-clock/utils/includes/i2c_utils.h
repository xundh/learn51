#ifndef __I2C_UTILS_H__
#define __I2C_UTILS_H__
#include <reg52.h>
#include "types.h"

sbit IIC_SCL = P2^1;
sbit IIC_SDA = P2^0;

/**
* @brief I2C 起始信号
*/
void i2c_start(void);

/**
* @brief I2C 停止信号
*/
void i2c_stop(void);

/**
* @brief I2C 应答信号
*/
void i2c_ack(void);

/**
* @brief I2C 非应答信号
*/
void i2c_nack(void);

/**
* @brief 主机等待从机应答
*/
u8 i2c_wait_ack(void);

/**
* @brief I2C 发送一个字节
*/
void i2c_send_byte(u8 dat);

/**
* @brief I2C 读取一个字节
* @param ack 0:非应答 1:应答
*/
u8 i2c_read_byte(u8 ack);
#endif // __I2C_UTILS_H__

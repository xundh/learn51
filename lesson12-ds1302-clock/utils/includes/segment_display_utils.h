#ifndef __SEGMENT_DISPLAY_UTILS_H__
#define __SEGMENT_DISPLAY_UTILS_H__
#include <reg52.h>
#include "types.h"

void segment_display();
/**
* @brief 单个数码管显示一个数字
* @param position 数码管位置,从1-8
* @param num 数码管显示数字
*/
void segment_show_u8(u8 position, u8 num);

/**
* @brief 数码管显示 int 数值
* @param number 数值
*/
void segment_show_int(int numbers);
void segment_show_u8_array(u8 *num);
#endif // __SEGMENT_DISPLAY_UTILS_H__
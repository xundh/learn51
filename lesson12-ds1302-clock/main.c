#include <reg52.h>
#include "led_utils.h"
#include "common_utils.h"
#include "timer_utils.h"
#include "uart_utils.h"
#include "key_utils.h"
#include "eeprom_utils.h"
#include "segment_display_utils.h"
#include "ds1302.h"


/**
* @brief 主函数
*/
main()
{
	u8 time_buf[8];
	// 关闭所有led
	led_all_off();
	uart_init(0xFA);
	ds1302_init(); 
	
	while(1)
	{
		ds1302_read_time();
		uart_send(u8_to_hex(gDS1302_TIME[2]));
		time_buf[0]=gDS1302_TIME[2] / 16;
		time_buf[1]=gDS1302_TIME[2] & 0x0f;
		time_buf[2]=0x10;
		time_buf[3]=gDS1302_TIME[1] / 16;
		time_buf[4]=gDS1302_TIME[1] & 0x0f;
		time_buf[5]=0x10;
		time_buf[6]=gDS1302_TIME[0] / 16;
		time_buf[7]=gDS1302_TIME[0] & 0x0f;

		segment_show_u8_array(time_buf);
	}
}

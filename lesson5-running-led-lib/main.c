#include "reg52.h"
#include "types.h"
#include "intrins.h"

// 定义LED的端口为P2
#define LED_PORT P2         

// 延时函数，延时一段时间
void delay_10us(u16 ten_us){
    while(ten_us--);        
}

void main(){
	int i=0;
	bit direction=0;
    while(1){
        // 
    	LED_PORT = _crol_(0xFE, i);
		if(direction==0){
		 	i++;
			if(i>=7){
				direction=1;
				i=7;
			}
		}else{
			i--;
			if(i<0){
				i=1;
				direction=0;
			}
		}

        delay_10us(50000*(i+1));
    }
}